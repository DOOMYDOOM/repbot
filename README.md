# Repbot
**This bot aims to give users a form of reputation on servers, be it good, or bad.**

### To get things started  
* Set up a bot account  
* Clone the repository and install dependencies  
    * `git clone git@gitlab.com:DOOMYDOOM/repbot.git`
    * `cd repbot`
    * `npm install`
* You'll need to edit the example_settings.json   
    * Insert your bot token into the token field  
    * "token": "token goes here"  
    * Now set the cooldown time
    * Now save the file as settings.json
* Now open up bot_repbot.js
    * Change channel ids to your corresponding ids
    * Change bot owner id to your id
* Find example_reputation.sqlite
    * Rename it to reputation.sqlite
* Invite your bot 
    * go to discordapp.com/developers
    * click applications
        * find and copy client id
        * replace "CLIENT ID" with your client id 
            * https://discordapp.com/oauth2/authorize?client_id=`CLIENTID`&scope=bot
* Starting repbot
    * `pm2 start bot_repbot.js`
* Killing the session
*   * `pm2 kill`